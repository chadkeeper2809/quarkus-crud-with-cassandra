package crud.cassandra.entity.mapper;

import com.datastax.oss.driver.api.mapper.annotations.DaoFactory;
import com.datastax.oss.driver.api.mapper.annotations.Mapper;
import crud.cassandra.dao.UserDao;

@Mapper
public interface UserMapper {
    @DaoFactory
    UserDao userDao();
}
