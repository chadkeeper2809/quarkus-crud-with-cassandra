package crud.cassandra.resource;

import crud.cassandra.dto.UserDto;
import crud.cassandra.entity.User;
import crud.cassandra.service.UserService;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    @Inject
    UserService userService;

    @GET
    public List<UserDto> getAll(){
        return userService.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @GET
    @Path("{id}")
    public UserDto getOne(@PathParam("id") int id){
        return convertToDto(userService.fineOne(id));
    }

    @POST
    public void add(UserDto userDto){
        userService.save(convertFromDto(userDto));
    }

    private UserDto convertToDto(User user){
        return new UserDto(user.getId(), user.getFirstName(), user.getLastName(), user.getPhoneNumber());
    }

    private User convertFromDto(UserDto userDto){
        return new User(userDto.getId(), userDto.getFirstName(), userDto.getLastName(), userDto.getPhoneNumber());
    }
}
