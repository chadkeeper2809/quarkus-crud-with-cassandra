package crud.cassandra.service;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.PreparedStatement;
import com.datastax.oss.driver.api.core.cql.Row;
import com.datastax.oss.driver.api.core.cql.SimpleStatement;
import crud.cassandra.dao.UserDao;
import crud.cassandra.entity.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class UserService {

    private final CqlSession cqlSession;

    @Inject
    UserDao userDao;

    public UserService(CqlSession session){
        this.cqlSession = session;
    }

    public void save(User user){
        userDao.update(user);
    }

    public List<User> getAll(){
        return userDao.findAll().all();
    }

    public User fineOne(int id){
        PreparedStatement preparedStatement = cqlSession.prepare("SELECT * FROM user WHERE id = ?");

        Row row = cqlSession.execute(preparedStatement.bind(id)).one();

        if (row == null){
            return null;
        }

        return new User(row.getInt("id"), row.getString("first_name"), row.getString("last_name"), row.getString("phone_number"));
    }
}
